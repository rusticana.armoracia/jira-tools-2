function read (blob) {
    const reader = new FileReader();

    return new Promise(resolve => {
        reader.onloadend = () => {
            const result = reader.result;
            resolve(result);
        };
        reader.readAsText(blob);
    });
}

function traverse(object, callback, parent, spawnChild) {
    for(let [key, value] of Object.entries(object)) {
        if (typeof value === 'object' && !!value) {
            let child = spawnChild(parent, key);
            traverse(value, callback, child, spawnChild);
        }
        else {
            callback(key, value, parent);
        }
    }
}

function createFieldSet (label, value, parent) {
    let div = document.createElement('div');
    div.classList.add('field-set');

    div.innerHTML = `<label>
            <span class="label">${label}</span>
            <input type="text" value="${value}"/>
        </label>`;

    parent.appendChild(div);
}

function spawnContainer(parent, label) {
    let div = document.createElement('div');
    div.classList.add('field-set');
    div.classList.add('container');

    div.innerHTML = `<label>${label}</label>`;

    parent.appendChild(div);
    return div;
}

function collect(node, data) {
    [...node.children]
        .filter(item => item.classList.contains('field-set'))
        .forEach(item => {
            data[item.querySelector('label').innerText.trim()] = item.classList.contains('container') ?
                collect(item, {}) :
                item.querySelector('input').value
        });

    return data;
}

document.querySelector('button#config').addEventListener('click', async () => {
    const directoryHandle = await window.showDirectoryPicker();

    const fileHandle =  await directoryHandle.getFileHandle('options-2.json');

    const file = await fileHandle.getFile();
    const text = await read(file);

    let config = JSON.parse(text);

    traverse(config, createFieldSet, document.querySelector('div#result'), spawnContainer);

    const button = document.createElement('button');
    button.innerText = 'save';
    document.querySelector('div#result').appendChild(button);

    button.addEventListener('click', async () => {
        let data = JSON.stringify(collect(document.querySelector('div#result'), {}));

        const stream = await fileHandle.createWritable({keepExistingData: false});

        const result = await stream.write(new Blob([data], {type: 'text/plain'}));
        
        await stream.close();

        window.location.reload();
    });
});