import {chromeApi} from '../lib/chrome/chromeApi.js';
import {jiraApi} from '../lib/jira/jiraApi.js';
import {configPromise} from './getConfig.js'

(async () => {
    let options = await configPromise;

    document.querySelector('button#board').addEventListener('click', async () => {
        let boardId = prompt('Enter board id');
        let tab = await chromeApi.tab.open(options.jira.baseUrl);
        let isAuth = await chromeApi.execute.asContextScript(tab.id, jiraApi.isAuth, options.jira.baseUrl);
        if (!isAuth) {
            alert('unauthorised');
            chromeApi.tab.close(tab);
            return;
        }
        let sprints = await chromeApi.execute.asContextScript(tab.id, jiraApi.getSprints, options.jira.baseUrl, boardId);
        console.log(sprints);
        chromeApi.tab.close(tab);
    });
})();
