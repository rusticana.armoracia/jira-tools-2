import {chromeApi} from '../lib/chrome/chromeApi.js';
import {jiraApi} from '../lib/jira/jiraApi.js';

/**
 * @param jql
 * @param fields
 * @param authenticatedJiraTabId
 * @param baseUrl
 * @returns {Promise<Array>}
 */
export async function timeInStatus(jql, fields, authenticatedJiraTabId, baseUrl) {
    const allFields = await chromeApi.execute.asContextScript(authenticatedJiraTabId, jiraApi.getAllFields, baseUrl);
    const statuses = await chromeApi.execute.asContextScript(authenticatedJiraTabId, jiraApi.getAllStatuses, baseUrl);
    const issues = await chromeApi.execute.asContextScript(authenticatedJiraTabId, 
            jiraApi.search, 
            baseUrl,
            jql,
            {
                fields: fields.map(alias => jiraApi.local.getField(alias, allFields).id),
                history: true,
            });
    jiraApi.local.timeInStatus(issues, statuses);

    return issues;
}
