import {configPromise} from './getConfig.js';
import {download} from '../lib/common/download.js'
import {BurnupGraph} from "../lib/components/BurnupGraph.js";
import {JiraTab} from "../lib/jira/JiraTab.js";
import {VelocityChart} from "../lib/components/VelocityChart.js";

/**
 * @param {[]} fields
 * @param {JiraTab} jiraTab
 * @param {[]} issues
 * @returns {*[]}
 */
async function createTableContent(fields, jiraTab, issues) {
    let statuses = [];
    issues.forEach(item => item.timeInStatus.forEach(({status, duration, millis}) => {
        if (statuses.indexOf(status) === -1) {
            statuses.push(status);
        }
    }));

    const configStatuses = config.teamReport.statuses;
    if (configStatuses.length !== 0) {
        statuses = configStatuses;
    }

    let descriptors = await Promise.all(fields.map(async item => await jiraTab.local.getField(item)));

    let values = issues.map(async item => {
        let values = descriptors.map(async descriptor => await jiraTab.local.renderField(item, descriptor.id));
        return [
            ...(await Promise.all(values)),
            ...statuses.map(status => {
                let match = item.timeInStatus.filter(entry => entry.status == status)[0];

                if (!match) {
                    return ' ';
                }
                return match.millis;
            })
        ]
    });
    return [[...descriptors.map(item => item.name), ...statuses], ...(await Promise.all(values))];
}

let storedData = null;
let config = null;
let allFields = [];
let sprints = [];
let scrumBoardId = null;
/**
 * @type {JiraTab}
 */
let jiraTab = null;

async function renderCellContent(data, isKey, isFirst) {
    if (typeof data === "number") {
        return JiraTab.toDuration(data);
    }
    if (isKey && !isFirst) {
        return `<a target="_blank" href="${config.jira.baseUrl}/browse/${data}">${data}</a>`;
    }
    return data;
}

/**
 * @param {[]}data
 * @param {number} selectedColumnIndex

 * @returns {string}
 */
async function renderTableContent(data, selectedColumnIndex = -1) {
    let field = await jiraTab.local.getField('issuekey');
    const keyIndex = data[0].indexOf(field.name);

    let getRow = async (row, rowIdnex) => {
        let getCell = async (cell, columnIndex) => {
            return `${rowIdnex === 0 ? `<th ${selectedColumnIndex * 1 == columnIndex ?
                'class="selected"' : ''} data-column-index="${columnIndex}">` :
                `<td data-value="${cell}">`}${await renderCellContent(cell, keyIndex === columnIndex, rowIdnex === 0) }${rowIdnex === 0 ?
                '</th>' : '</td>'}`
        };
        return `<tr>
        ${(await Promise.all(row.map(getCell))).join('')}
    </tr>`
    };
    return `<tbody>
        ${(await Promise.all(data.map(getRow))).join('')}
    </tbody`;
}

(async () => {
    config = await configPromise;

    const {
        jira: {baseUrl: jiraUrl},
        teamReport: {teams, jql, fields, epicLinkField}
    } = config;

    const options = [
        (() => {
            const emptyOption = document.createElement('option');

            emptyOption.innerText = ' ';
            emptyOption.value = null;

            return emptyOption;
        })(),
        ...teams.map(item => {
            const html = document.createElement('option');
            html.innerText = item.name;
            html.value = item.project;

            return html;
        })
    ];

    const select = document.querySelector('select#team');

    options.forEach(item => select.appendChild(item));

    document.querySelector('button#download').addEventListener('click', () => {
        download(storedData
            .map(row => row.map(item => typeof item === 'number' ? Math.floor(item / 1000 / 60 / 60 / 24) : item)
            .join(';')).join('\n'), 'time in status.csv', 'text/csv');
    });

    document.querySelector('button#show-sprints').addEventListener('click', async () => {
        let graphs = sprints
            .filter(item => item.state !== 'future')
            .map(sprint => new BurnupGraph(scrumBoardId, sprint.id));

        graphs = await Promise.all(graphs);

        graphs.forEach(item => item.appendTo(document.querySelector('#burnup-container')));
    });

    document.querySelector('button#show-velocity').addEventListener('click', async () => {
        const chart = await new VelocityChart(scrumBoardId);
        chart.appendTo(document.querySelector('#velocity-container'));
        console.log(chart);
    });

    document.querySelector('table#time-in-status').addEventListener('click', async e => {
        if (e.target.nodeName.toLowerCase() === 'th') {
            const index = e.target.getAttribute('data-column-index');

            if (e.target.classList.contains('selected')) {
                return;
            }

            const sortedData = [
                storedData[0],
                ...storedData
                    .slice(1)
                    .sort((a, b) => {
                        if (typeof a[index] === 'number' && typeof b[index] === "number") {
                            return b[index] - a[index];
                        }
                        else if (typeof a[index] === 'string' && typeof b[index] === "string") {
                            return a[index] === ' ' ? 1 : b[index] === ' ' ? -1 :
                                a[index] > b[index] ? 1 : -1;
                        }
                        else if (typeof a[index] !== 'number') {
                            return 1;
                        }
                        else if (typeof b[index] !== 'number') {
                            return -1;
                        }
                        else {
                            return 0;
                        }
                    })
            ];

            document.querySelector('table#time-in-status').innerHTML = await renderTableContent(sortedData, index);
        }
    });

    select.addEventListener('change', async (e) => {
        document.querySelector('#result').classList.add('hidden');
        storedData = null;
        const selected = [...e.target.selectedOptions];

        if (selected.length > 0 && selected[0].value) {
            const value = selected[0].value;
            const team = teams.filter(item => item.project == value)[0];

            if (team) {
                const {project} = team;
                scrumBoardId = team.scrumBoardId;
                //cleanup
                document.querySelector('#burnup-container').innerHTML = '';
                document.querySelector('#velocity-container').innerHTML = '';

                /**
                 * @type JiraTab
                 */
                jiraTab = await new JiraTab();
                if (!await jiraTab.isAuth()) {
                    alert('unauthorised');
                    await jiraTab.close();
                    return;
                }

                sprints = await jiraTab.getSprints(scrumBoardId);

                const sprintId = [
                    ...sprints.filter(item => item.state === 'active').reverse(),
                    ...sprints.filter(item => item.state === 'closed').reverse()
                ][0].id;

                let issuesInStatus = await jiraTab.timeInStatus(`${jql} AND project = ${project}`, [...fields, epicLinkField]);
                allFields = await jiraTab.getAllFields();

                const epicFieldDescriptor = await jiraTab.local.getField(epicLinkField);
                const epicsOfIssues = issuesInStatus
                    .map(item => item.fields[epicFieldDescriptor.id])
                    .reduce((accum, item) => {
                        if (!accum.includes(item)) {
                            accum.push(item);
                        }
                        return accum;
                    }, [])
                    .filter(item => item !== null);

                const epics = await jiraTab.search(`issue in(${epicsOfIssues.join(', ')})`, {fields: ['issuekey', 'summary']});

                issuesInStatus = await Promise.all(issuesInStatus.map(async item => {
                    const oldValue = item.fields[epicFieldDescriptor.id];
                    const epic = epics.filter(item => item.key === oldValue)[0];
                    item.fields[epicFieldDescriptor.id] = oldValue === null ? ' ' :
                        await jiraTab.local.renderField(epic, 'issuekey') + ' '
                        + await jiraTab.local.renderField(epic, 'summary');

                    return item;
                }));

                storedData = await createTableContent([...fields, epicLinkField], jiraTab, issuesInStatus);

                document.querySelector('#velocity').setAttribute('href', jiraTab.local.getVelocityChartUrl(scrumBoardId));
                document.querySelector('#burnup').setAttribute('href', jiraTab.local.getSprintBurnupUrl(scrumBoardId, sprintId));
                document.querySelector('#cfd').setAttribute('href', jiraTab.local.getCumulativeFlowDiagramUrl(scrumBoardId));
                document.querySelector('table#time-in-status').innerHTML = await renderTableContent(storedData, -1);
                let result = Promise.all(epics.map(async row => {
                    const epicText = `${await jiraTab.local.renderField(row, 'issuekey')} ${await jiraTab.local.renderField(row, 'summary')}`;
                    const issues = issuesInStatus.filter(item => item.fields[epicFieldDescriptor.id] === epicText);
                    const toDo = issues.filter(item => item.fields.status.statusCategory.key === 'new').length;
                    const inProgress = issues.filter(item => item.fields.status.statusCategory.key === 'indeterminate').length;
                    const done = issues.filter(item => item.fields.status.statusCategory.key === 'done').length;

                    return `
                <tr>
                    <td>${epicText}</td>
                    <td class="to-do">${toDo}</td>
                    <td class="in-progress">${inProgress}</td>
                    <td class="done">${done}</td>
                </tr>`
                }));
                document.querySelector('table#epic-progress').innerHTML = `
<tbody>
    <tr>
        <th>Issues in epics by status category</th>
        <th>To do</th>
        <th>In Progress</th>
        <th>Done</th>
    </tr>
    ${(await (result)).join('\n')}
</tbody>`;

                document.querySelector('#result').classList.remove('hidden');
                return;
            }
        }

        document.querySelector('#velocity').removeAttribute('href');
        document.querySelector('#burnup').removeAttribute('href');
        document.querySelector('#cfd').removeAttribute('href');
        document.querySelector('table#time-in-status').innerHTML = '';
        document.querySelector('table#epic-progress').innerHTML = '';
        document.querySelector('#burnup-container').innerHTML = '';

        document.querySelector('#result').classList.add('hidden');
    })
})();