import { GET } from '../lib/http/GET.js';

export const configPromise = new Promise(async resolve => resolve(JSON.parse(await GET('../options-2.json'))));