import {chromeApi} from '../lib/chrome/chromeApi.js';
import {jiraApi} from '../lib/jira/jiraApi.js';
import {configPromise} from './getConfig.js'

//todo: rename file

//todo: use extracted function
function download (string, fileName) {
    const BOM = new Uint8Array([0xEF,0xBB,0xBF]);
    const blob = new Blob([BOM, string], { type: 'text/csv;charset=utf-8;', encoding:"UTF-8" });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.setAttribute("href", url);
    link.setAttribute("download", fileName);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    URL.revokeObjectURL(url);
}

function getDays(issue, status) {
    let match = issue.timeInStatus.filter(entry => entry.status == status)[0];

    if (!match) {
        return ' ';
    } 
    
    return Math.floor(match.millis / 1000 / 60 / 60 / 24);
}

function createCSV(issues, allFields, fields) {
    let statuses = [];
    issues.forEach(item => item.timeInStatus.forEach(({status, duration, millis}) => {
        if (statuses.indexOf(status) === -1) {
            statuses.push(status);
        }
    }));

    let descriptors = fields.map(item => jiraApi.local.getField(item, allFields));

    let rows = [[...descriptors.map(item => item.name), ...statuses], ...issues.map(item => {
        return [
            ...descriptors.map(descriptor => jiraApi.local.renderField(item, descriptor.name, allFields)),
            ...statuses.map(status => getDays(item, status))
        ]
    })];

    return rows.map(item => item.join(';')).join('\n');
}

(async () => {
    let options = await configPromise;

    document.querySelector('button#status-changes').addEventListener('click', async () => {
        let baseUrl = options.jira.baseUrl;
        if (baseUrl === '') {
            baseUrl = prompt('Enter jira base url');
        }
        const tab = await chromeApi.tab.open(baseUrl);
        const isAuth = await chromeApi.execute.asContextScript(tab.id, jiraApi.isAuth, baseUrl);
        if (!isAuth) {
            alert('unauthorised');
            chromeApi.tab.close(tab);
            return;
        }

        const allFields = await chromeApi.execute.asContextScript(tab.id, jiraApi.getAllFields, baseUrl);

        let fields = options.timeInStatus.fields
            .split(',')
            .map(item => item.trim());
        if (fields === '') {
            fields = prompt('Enter fields')
                .split(',')
                .map(item => item.trim());
        }

        const jql = options.timeInStatus.jql || prompt('Enter JQL to find for issues');

        const issues = await chromeApi.execute.asContextScript(tab.id, 
            jiraApi.search, 
            baseUrl,
            jql,
            {
                fields: fields.map(alias => jiraApi.local.getField(alias, allFields).id),
                history: true,
            });

        const statuses = await chromeApi.execute.asContextScript(tab.id, jiraApi.getAllStatuses, baseUrl);
        
        jiraApi.local.timeInStatus(issues, statuses);

        console.info(issues);
        chromeApi.tab.close(tab);

        download(createCSV(issues, allFields, fields), 'time-in-status.csv');
    });
})();