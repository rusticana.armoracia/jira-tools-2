import {chromeApi} from '../lib/chrome/chromeApi.js';

(async () => {
    console.log('I am background');

    chrome.commands.onCommand.addListener(async function(command) {
    console.log('Command:', command);
    switch (command) {
        case 'show-plugin-page': {
            let tab = await chromeApi.tab.open('../pages/playground.html');
            chromeApi.tab.focus(tab);

            console.log(tab);

            break;
        }
        default: break;
    }
    });

})();