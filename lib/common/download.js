export function download (fileContent, fileName, type) {
    const BOM = new Uint8Array([0xEF,0xBB,0xBF]);
    const blob = new Blob([BOM, fileContent], { type: `${type};charset=utf-8;`, encoding:"UTF-8" });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.setAttribute("href", url);
    link.setAttribute("download", fileName);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    URL.revokeObjectURL(url);
}
