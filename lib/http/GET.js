/**
 * @param {string} url
 * @returns {Promise<string>}
 */
export async function GET(url) {
    return new Promise(resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onreadystatechange = (() => {
            console.info(`GET status ${xhr.status}, readystate ${xhr.readyState}`);
            if (xhr.status === 200 && xhr.readyState === 4) {
                console.info(xhr.responseText);
                resolve(xhr.responseText);
                console.info(`GET done URL: ${url}`)
            }
        });
        xhr.send();
        console.info(`GET starting URL: ${url}`);
    })
}