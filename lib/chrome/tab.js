

/**
 * @param {string} url
 * @returns {Promise<{id: number}>}
 */
export  function openTab(url) {
    return new Promise(resolve => {
        chrome.tabs.create({ active: false, url }, resolve);
    });
}

export async function focusTab(tab) {
    await new Promise(resolve => chrome.windows.update(tab.windowId, { focused: true }, resolve));
    return new Promise(resolve => chrome.tabs.update(tab.id, { active: true }, resolve));
}

export async function closeTab(tab) {
    return new Promise(resolve => chrome.tabs.remove(tab.id, resolve));
}
