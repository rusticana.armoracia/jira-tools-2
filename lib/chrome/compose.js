function smartToString(entry) {
    switch (typeof entry) {
        case 'function':
            return entry.toString();
        case 'string': 
            return `'${entry}'`;
        case 'number':
            return entry;
        case 'object'://todo: checks for dufferent cases
            return JSON.stringify(entry);
        default:
            throw new Error('smart to string not implemented');
    }
}

/**
 * @param fn
 * @param provides
 * @returns {function(*, ...[*])}
 */
export function compose(fn, ...provides) {
    return (resolve, ...params) => {
        return `(() => {
            //Provides
            ${[...provides].map(smartToString).join(';\n\n')}

            //Invocation
            (${smartToString(fn)}).call(this, ${resolve}, ${[...params].map(smartToString).join(', ')});
        })();`;
    }
}
