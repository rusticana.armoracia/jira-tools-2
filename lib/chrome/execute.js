export function executeScript(tabId, fn, ...args) {
    return new Promise(resolve => {
        const random = Math.random().toString();
        const extensionId = chrome.runtime.getURL('')
            .replace('chrome-extension://', '')
            .replace('/', '');
        const listener = (message) => {
            let {random: randomSeed, payload} = message;
            console.info(randomSeed, random);
            if (randomSeed == random) {
                console.info('match!')
                chrome.runtime.onMessage.removeListener(listener);
                resolve(payload);
            }
        };
        chrome.runtime.onMessage.addListener(listener);

        let acknowledgeExtension = ((payload) => chrome.runtime.sendMessage('`${extensionId}`', {random: `${random}`,payload: payload}))
            .toString()
            .replace('`${random}`', random)
            .replace('`${extensionId}`', extensionId);

        const code = fn(acknowledgeExtension, ...args);

        console.info(code);

        chrome.tabs.executeScript(tabId, {code}, () => {});
    });
}