import {openTab, focusTab, closeTab} from './tab.js';
import {executeScript} from './execute.js';

export const chromeApi = {
    tab: {
        open: openTab,
        focus: focusTab,
        close: closeTab,
    },
    execute: {
        asContextScript: executeScript
    }
};