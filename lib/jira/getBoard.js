import {compose} from "../chrome/compose.js";
import {GET} from "../http/GET.js";

async function getBoard(resolve, baseUrl, boardId) {
    const raw = await GET(`${baseUrl}/rest/agile/1.0/board/${boardId}`);

    resolve(JSON.parse(raw));
}

export const composed = compose(getBoard, GET);