import { compose } from "../chrome/compose.js";
import { GET } from "../http/GET.js";

async function getSprints (resolve, baseUrl, boardId) {
    let result = await GET(`${baseUrl}/rest/agile/1.0/board/${boardId}/sprint`);

    result = JSON.parse(result).values;
    
    resolve (result);
}

/**
 * @function composeGetSprints
 * @param {string} baseUrl
 * @param {number} boardId
 * @returns {Promise<[]>}
 */
export const composeGetSprints = compose(getSprints, GET);
