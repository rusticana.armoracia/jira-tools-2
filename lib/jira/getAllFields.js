import {compose} from '../chrome/compose.js';
import {GET} from '../http/GET.js';

async function getAllFields(resolve, baseUrl) {
    const result = await GET(`${baseUrl}/rest/api/2/field`);

    resolve (JSON.parse(result));
}

export const composeGetAllFields = compose(getAllFields, GET);