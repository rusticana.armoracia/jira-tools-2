import {compose} from "../chrome/compose.js";
import {GET} from "../http/GET.js";


/**
 * @param {function} resolve
 * @param {string} baseUrl
 * @param {number} boardId
 * @param {number} sprintId
 * @returns {Promise<Object>}
 */
async function getBurndownData(resolve, baseUrl, boardId, sprintId) {
    const raw = await GET(`${baseUrl}/rest/greenhopper/1.0/rapid/charts/scopechangeburndownchart?rapidViewId=${boardId}&sprintId=${sprintId}`);

    const result = JSON.parse(raw);

    resolve(result);
}

export const composed = compose(getBurndownData, GET);