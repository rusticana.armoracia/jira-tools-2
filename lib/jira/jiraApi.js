import {composeIsAuth} from './isAuth.js';
import {composeGetSprints} from './getSprints.js';
import {composed as getBurndownData} from './getBurndownData.js';
import {composeSearchIssues} from './search.js';
import {composeGetAllStatuses} from './getAllStatuses.js';
import {composeGetAllFields} from './getAllFields.js';
import {composed as getBoard} from './getBoard.js'
import {composed as getSprint} from './getSprint.js'

import {timeInStatus, toDuration} from './local/timeInStatus.js';
import {renderField, getField} from './local/field.js';
import {getSprintBurnupUrl, getVelocityChartUrl, getCumulativeFlowDiagramUrl} from './local/reporting.js'

export const jiraApi = {
    isAuth: composeIsAuth,
    getSprints: composeGetSprints,
    getBurndownData,
    getBoard,
    getSprint,
    search: composeSearchIssues,
    getAllStatuses: composeGetAllStatuses,
    getAllFields: composeGetAllFields,
    local: {
        timeInStatus,
        toDuration,
        renderField,
        getField,
        getSprintBurnupUrl,
        getVelocityChartUrl,
        getCumulativeFlowDiagramUrl,
    },
};