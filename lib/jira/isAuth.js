import { compose } from "../chrome/compose.js";
import { GET } from "../http/GET.js";

async function isAuth(resolve, baseUrl) {
    let result = await GET(`${baseUrl}/rest/api/2/myself`);

    resolve (!!JSON.parse(result).self);
}

export const composeIsAuth = compose(isAuth, GET);