import {compose} from "../chrome/compose.js";
import {GET} from "../http/GET.js";

async function getSprint(resolve, baseUrl, sprintId) {
    const raw = await GET(`${baseUrl}/rest/agile/1.0/sprint/${sprintId}`);

    resolve(JSON.parse(raw));
}

export const composed = compose(getSprint, GET);