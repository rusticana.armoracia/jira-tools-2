import {compose} from '../chrome/compose.js';
import {GET} from '../http/GET.js';

async function searchIssues(resolve, baseUrl, jql, {
    max = 300,
    chunkSize = 50,
    fields = ['key', 'status', 'summary', 'assignee', 'issuetype', 'created'],
    history = false}) {

        let startAt = 0;
        let maxResults = chunkSize;
        let total = 0;
        let done = false;
        let issues = [];

        let spawnRequest = () => GET(`${baseUrl}/rest/api/2/search?jql=${jql}${history ? '&expand=changelog' : ''}&fields=${fields.join(',')}&startAt=${startAt}&maxResults=${maxResults}`);
        let iterator = {
            [Symbol.asyncIterator]() {
                return {
                    async next() {
                        return {
                            value: spawnRequest(),
                            done
                        }
                    }
                }
            }
        };

        for await (let stream of iterator) {
            let chunk = JSON.parse(await stream);
            total = chunk.total;
            if (chunk.startAt + maxResults >= total) {
                done = true;
            }
            startAt += maxResults;
            issues = [...issues, ...chunk.issues];

            if (startAt > max) {
                throw new Error('max issue count exceeded');
            }

        }
        console.info(issues);
        
        if (history) {
            const historyEntries = issues.map(issue => {
                return issue.changelog.histories.map(history => {
                    return history.items.map(item => {
                        return {
                            issueId: issue.id,
                            key: issue.key,
                            summary: issue.fields.summary,
                            changeId: history.id,
                            author: history.author.key,
                            created: history.created,
                            timestamp: new Date(history.created).getTime(),
                            field: item.field,
                            from: item.from,
                            to: item.to
                        }
                    }).flat()
                }).flat()
            }).flat();

            console.info(historyEntries);

            issues.forEach(item => item.history = historyEntries.filter(entry => entry.key === item.key));
        }

        resolve(issues);
}

export const composeSearchIssues = compose(searchIssues, GET);