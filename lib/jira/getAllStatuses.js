import { compose } from "../chrome/compose.js";
import { GET } from "../http/GET.js";

async function getAllStatuses(resolve, baseUrl) {
    let result = await GET(`${baseUrl}/rest/api/2/status`);

    resolve (JSON.parse(result));
}

export const composeGetAllStatuses = compose(getAllStatuses, GET);
