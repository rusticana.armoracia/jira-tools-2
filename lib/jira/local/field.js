/**
 * @param {object} issue
 * @param {string} alias
 * @param {Array} allFields
 * @returns {string}
 */
export function renderField(issue, alias, allFields) {
    const field = getField(alias, allFields);
    switch (alias.toLowerCase()) {
        case 'key':
        case 'issuekey':
            return issue.key;
        case 'summary': // ...or many standard fields
            return issue.fields.summary;
        case 'status': // ...or some other standard fields
        case 'project': 
            return issue.fields[alias.toLowerCase()].name;
        case 'epic link': // ... or some custom fields
            return issue.fields[field.id];
        case 'created': { //...or other date fields
            const date = new Date(issue.fields[getField(alias, allFields).id]);
            return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`; //todo: add some convenient date-time library
        }
        default:
            return issue.fields[getField(alias, allFields).id];
    }
}

export function getField(alias, allFields) {
    let lowerCase = alias.toLowerCase();
    const standardFieldDescriptor = allFields.filter(item => item.id.toLowerCase() ===  lowerCase)[0];
    const customFieldDescriptor = allFields.filter(item => item.name.toLowerCase() ===  lowerCase)[0];

    return standardFieldDescriptor || customFieldDescriptor || null;
}