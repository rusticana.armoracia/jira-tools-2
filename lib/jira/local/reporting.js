export function getVelocityChartUrl(baseUrl, boardId) {
    return `${baseUrl}/secure/RapidBoard.jspa?rapidView=${boardId}&view=reporting&chart=velocityChart`;
}

export function getSprintBurnupUrl(baseUrl, boardId, sprintId) {
    return `${baseUrl}/secure/RapidBoard.jspa?rapidView=${boardId}&view=reporting&chart=burnupChart&sprint=${sprintId}`;
}

export function getCumulativeFlowDiagramUrl(baseUrl, boardId) {
    return `${baseUrl}/secure/RapidBoard.jspa?rapidView=${boardId}&view=reporting&chart=cumulativeFlowDiagram`;
}
