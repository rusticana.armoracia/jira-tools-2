export function toDuration(time) {
    let millis = time % 1000;
    time = Math.floor(time / 1000);
    let sec = time % 60;
    time = Math.floor(time / 60);
    let min = time % 60;
    time = Math.floor(time / 60);
    let hrs = time % 24;
    time = Math.floor(time / 24);
    let days = time % 7;
    time = Math.floor(time / 7);
    let weeks = time;

    let string = (weeks ? `${weeks}w ` : '') +
        (days ? `${days}d ` : '') +
        (hrs ? `${hrs}h ` : '') +
        (min ? `${min}m ` : '');

    return string.trim() || '0';
}

export function timeInStatus(issues, statuses) {
    const now = Date.now();
    const STATUS = statuses.reduce((accum, item) => {
        accum[item.id] = item.name;

        return accum;
    }, {});

    issues.forEach(item => {
        let changes = item.history.map(history => {
            return {
                timestamp: new Date(history.created).getTime(),
                field: history.field,
                from: history.from,
                to: history.to
            }
        });
        changes = [
            {
                timestamp: new Date(item.fields.created).getTime(),
                field: 'status',
                from: null,
                to: null
            }, 
            ...changes, 
            {
                timestamp: now,
                field: 'status',
                from: item.fields.status.id,
                to: null
            }
        ]
            .filter(item => item.field === 'status')
            .sort((a, b) => a.timestamp - b.timestamp);

        changes[0].to = changes[1].from;

        item.timeInStatus = changes.reduce((accum, item, index) => {
            let nextItem = changes[index + 1];
            if (!nextItem) {
                return accum;
            }
            let status = STATUS[nextItem.from];
            let duration = toDuration(nextItem.timestamp - item.timestamp);

            accum = [...accum, {status, duration, millis: nextItem.timestamp - item.timestamp}];
            return accum;
        }, []);
    });
}