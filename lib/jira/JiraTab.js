import {configPromise} from '../../actions/getConfig.js'
import {chromeApi} from "../chrome/chromeApi.js";
import {jiraApi} from "./jiraApi.js";


/**
 * class JiraTab
 */
export class JiraTab {

    static toDuration(millis) {
        return jiraApi.local.toDuration(millis);
    }

    #allFields;
    #allStatuses;

    baseUrl;
    tab;
    local;

    constructor() {
        const me = this;

        return new Promise(async resolve => {
            const {jira: {baseUrl}} = await configPromise;

            this.baseUrl = baseUrl;
            this.tab = await chromeApi.tab.open(`${baseUrl}/issues/?jql=`);

            this.local = {
                timeInStatus: async (issues) => jiraApi.local.timeInStatus(issues, await this.getAllStatuses()),
                renderField: async(issue, alias) => jiraApi.local.renderField(issue, alias, await this.getAllFields()),
                getField: async(alias) => jiraApi.local.getField(alias, await this.getAllFields()),

                getVelocityChartUrl: (boardId) => jiraApi.local.getVelocityChartUrl(this.baseUrl, boardId),
                getSprintBurnupUrl: (boardId, sprintId) => jiraApi.local.getSprintBurnupUrl(this.baseUrl, boardId, sprintId),
                getCumulativeFlowDiagramUrl: (boardId) => jiraApi.local.getCumulativeFlowDiagramUrl(this.baseUrl, boardId),
            };

            resolve(this);
        });
    }

    close() {
        chromeApi.tab.close(this.tab);
    }

    async isAuth() {
        return chromeApi.execute.asContextScript(this.tab.id, jiraApi.isAuth, this.baseUrl);
    }

    async getAllFields() {
        if (!this.#allFields) {
            this.#allFields = await chromeApi.execute.asContextScript(this.tab.id, jiraApi.getAllFields, this.baseUrl);
        }
        return this.#allFields;
    }

    async getAllStatuses() {
        if (!this.#allStatuses) {
            this.#allStatuses = await chromeApi.execute.asContextScript(this.tab.id, jiraApi.getAllStatuses, this.baseUrl);
        }
        return this.#allStatuses;
    }

    async getSprints(boardId) {
        return chromeApi.execute.asContextScript(this.tab.id, jiraApi.getSprints, this.baseUrl, boardId);
    }

    async getBurndownData(boardId, sprintId) {
        return chromeApi.execute.asContextScript(this.tab.id, jiraApi.getBurndownData, this.baseUrl, boardId, sprintId);
    }

    async search (jql, {
        max = 300,
        chunkSize = 50,
        fields = ['key', 'status', 'summary', 'assignee', 'issuetype', 'created'],
        history = false}) {

        return chromeApi.execute.asContextScript(this.tab.id, jiraApi.search, this.baseUrl, jql, {max, chunkSize, fields, history});
    }

    async timeInStatus (jql, fields) {
        fields = (await Promise
            .all(fields.map(item => this.local.getField(item))))
            .map(item => item.id);

        const issues = await this.search(jql, {
            fields,
            history: true,
        });

        await this.local.timeInStatus(issues);

        return issues;
    }

    async getBoard(boardId) {
        return chromeApi.execute.asContextScript(this.tab.id, jiraApi.getBoard, this.baseUrl, boardId)
    }

    async getSprint(sprintId) {
        return chromeApi.execute.asContextScript(this.tab.id, jiraApi.getSprint, this.baseUrl, sprintId);
    }
}


