import {JiraTab} from '../jira/JiraTab.js';

/**
 * @class BurnupGraph
 *
 * @property {CanvasRenderingContext2D} canvas
 */
export class BurnupGraph {
    static defaults = {
        size: {
            width: 300,
            height: 150,
        },
        color: {
            scope: 'red',
            done: 'green',
            axi: 'black',
        }
    };

    data;
    meta;
    graphData;
    graphMeta;
    canvas;
    element;

    constructor(boardId, sprintId, config = BurnupGraph.defaults) {
        return new Promise(async resolve => {
            /**
             * @type JiraTab
             */
            const tab = await new JiraTab();
            const data = await tab.getBurndownData(boardId, sprintId);
            const board = await tab.getBoard(boardId);
            const sprint = await tab.getSprint(sprintId);
            await tab.close();
            this.config = config;

            this.loadData(data);

                this.canvas = document.createElement('canvas');
                this.element = document.createElement('div');
                this.element.classList.add('burnup-chart-wrap');
                this.element.innerHTML = `
                <h3>${board.name}</h3>
                <h4>${sprint.name}</h4>
                <span class="canvas-wrap"></span><br/>
                <span>Committed scope: ${this.graphMeta.committedScope.length }</span><br/>
                <span>Added scope: ${this.graphData.maxScope - this.graphMeta.committedScope.length}</span><br/>
                <span>Total scope: ${this.graphData.maxScope}</span><br/>
                <hr/>
                <span>Done, out of committed: ${this.graphMeta.committedAndDone.length}</span><br/>
                <span>Done, out of added: ${this.graphData.maxDone - this.graphMeta.committedAndDone.length}</span><br/>
                <span>Total done: ${this.graphData.maxDone}</span><br/>
                <a target="_blank" href="${tab.local.getSprintBurnupUrl(boardId, sprintId)}">View in Jira</a>
            `;
            this.element.querySelector('.canvas-wrap').appendChild(this.canvas);

            this.resize(this.config.size.width, this.config.size.height);

            resolve(this);
        });
    }

    loadData(data) {
        this.data = Object.entries(data.changes)
            .map(([timestamp, [entry]]) => {return {...entry, timestamp: timestamp * 1}})
            .sort((a, b) => a.timestamp - b.timestamp);
        this.meta = {
            start: data.activatedTime,
            end: data.completetime || data.endTime,
            now: Date.now()
        };

        const outOfBurnupChanges = this.data
            .filter(item => item.column && item.column.notDone === false && item.timestamp < data.activatedTime)
            .map(item => item.key);

        this.graphData = {
            minTime: Math.min(this.meta.start, /*...this.data.map(item => item.timestamp)*/),
            maxTime: Math.max(this.meta.end, /*...this.data.map(item => item.timestamp)*/),
            minScope: 0,
            maxScope: this.data
                .filter(item => item.added)
                .filter(item => !outOfBurnupChanges.includes(item.key))
                .length,
            minDone: 0,
            maxDone: this.data.filter(item => item.column && item.column.notDone === false)
                .filter(item => !outOfBurnupChanges.includes(item.key))
                .length,
            events: this.data
                .filter(item => item.added || item.column && item.column.notDone === false)
                .filter(item => !outOfBurnupChanges.includes(item.key))
                .map(item => {
                    return {
                        ...item,
                        type: item.added ? 'scope' : 'done',
                        delta: 1,
                    };
                  }),
        };

        let committedScope = this.graphData.events
            .filter(item => item.type === 'scope' && item.timestamp < this.meta.start)
            .map(item => item.key)
            .reduce((accum, item) => {
                if (!accum.includes(item)){
                    accum.push(item);
                }
                return accum;
            }, []);
        this.graphMeta = {
            committedScope,
            committedAndDone: this.graphData.events
                .filter(item => item.column && item.column.notDone === false)
                .map(item => item.key)
                .reduce((accum, item) => {
                    if (!accum.includes(item)){
                        accum.push(item);
                    }
                    return accum;
                }, [])
                .filter(item => committedScope.includes(item))
        };
    }

    render() {
        /**
         * @type CanvasRenderingContext2D;
         */
        const ctx = this.canvas.getContext('2d');

        this.clear(ctx);
        this.drawAxi(ctx);
        this.drawBurnup(ctx, 'scope');
        this.drawBurnup(ctx, 'done');



    };

    /**
     * @param {CanvasRenderingContext2D} ctx
     */
    clear(ctx) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     */
    drawAxi(ctx) {
        ctx.beginPath();
        ctx.strokeStyle = this.config.color.axi;
        ctx.moveTo(0, 0);
        ctx.lineTo(0, ctx.canvas.height);
        ctx.lineTo(ctx.canvas.width, ctx.canvas.height);
        ctx.stroke();
        ctx.closePath();
    }

    toScreenX(units) {
        const screenDelta = this.canvas.width;
        const unitsDelta = this.graphData.maxTime - this.graphData.minTime;
        const screenX = (units - this.graphData.minTime) * screenDelta   / unitsDelta;

        return screenX;
    }

    toScreenY(units) {
        const screenDelta = this.canvas.height;
        const unitsDelta = this.graphData.maxScope - this.graphData.minScope;

        const screenY = (units - 0) * screenDelta / unitsDelta;

        return this.canvas.height - screenY;
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     * @param {string} context
     */
    drawBurnup(ctx, context) {
        ctx.beginPath();
        ctx.strokeStyle = this.config.color[context];
        let currentValue = 0;
        this.graphData.events
            .filter(item => item.type === context)
            .forEach(item => {
                currentValue += item.delta;
                ctx.lineTo(this.toScreenX(item.timestamp), this.toScreenY(currentValue - 1));
                ctx.lineTo(this.toScreenX(item.timestamp), this.toScreenY(currentValue));
            });
        ctx.lineTo(this.toScreenX(Date.now()), this.toScreenY(currentValue));

        ctx.stroke();
        ctx.closePath();
    }

    resize(width, height) {
        this.canvas.width = width;
        this.canvas.height = height;

        this.render();
    }

    /**
     * @param {HTMLElement} element
     */
    appendTo(element) {
        element.appendChild(this.element);
    }
}