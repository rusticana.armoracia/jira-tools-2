import {JiraTab} from "../jira/JiraTab.js";

export class VelocityChart {
    static defaultConfig = {
        size: {
            width: 600,
            height: 150
        },
        spacing: {
            columns: 0.3,
            spints: 0.5
        },
        color: {
            committed: '#81ccc8',
            added: '#ccc',
            committedAndDone: '#6ccc7e',
            addedAndDone: '#9ccca5',
            text: '#000',
            axi: '#000',
            averageVelocity: '#999'
        }
    };

    /**
     * @type HTMLDivElement
     */
    element;
    /**
     * @type HTMLCanvasElement
     */
    canvas;
    data;

    constructor(boardId, config = VelocityChart.defaultConfig) {
        return new Promise(async resolve => {
            /**
             * @type JiraTab
             */
            const tab = await new JiraTab();
            const sprints = (await tab.getSprints(boardId))
                .filter(item => item.state === 'closed')
                .sort((a, b) => {
                    return new Date(a.completeDate).getTime() - new Date(b.completeDate).getTime()
                });
            const board = await tab.getBoard(boardId);
            const blocks = await Promise.all(sprints.map(async item => {
                const sprint = await tab.getSprint(item.id);
                const data = await tab.getBurndownData(boardId, item.id);

                return {sprint, data};
            }));

            this.loadData(board, blocks);

            this.canvas = document.createElement('canvas');
            this.element = document.createElement('div');
            this.element.classList.add('velocity-chart-wrap');
            this.element.innerHTML = `
                <h3>${this.data.board.name}</h3>
                <span class="canvas-wrap"></span><br/>
                <table></table>
                <a target="_blank" href="${tab.local.getVelocityChartUrl(boardId)}">View in Jira</a>
            `;
            this.element.querySelector('.canvas-wrap').appendChild(this.canvas);

            this.config = config;

            await tab.close();

            this.resize(this.config.size.width, this.config.size.height);

            resolve(this);
        })
    }

    resize(width, height) {
        this.canvas.width = width;
        this.canvas.height = height;

        this.render();
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     */
    clear(ctx) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }

    render() {
        /**
         * @type CanvasRenderingContext2D;
         */
        const ctx = this.canvas.getContext('2d');

        this.clear(ctx);
        this.drawAxi(ctx);
        this.drawVelocity(ctx);


    }


    /**
     * @param {CanvasRenderingContext2D} ctx
     */
    drawVelocity(ctx) {
        const sprintWidth = ctx.canvas.width /
            (this.data.sprints.length + (this.data.sprints.length - 1) * this.config.spacing.spints);
        const sprintSpacingWidth = sprintWidth * this.config.spacing.spints;
        const columnWidth = sprintWidth / (2 + this.config.spacing.columns);
        const columnSpacingWidth = columnWidth * this.config.spacing.columns;

        this.data.sprints.forEach((sprint, index) => {
            const committed = sprint.committed.length;
            const added = sprint.added.length;
            const committedAndDone = sprint.committedAndDone.length;
            const addedAndDone = sprint.addedAndDone.length;

            const firstColumnLeft = index * sprintWidth + index * sprintSpacingWidth;
            const secondColumnLeft = firstColumnLeft + columnWidth + columnSpacingWidth;
            ctx.fillStyle = this.config.color.committed;
            ctx.fillRect(firstColumnLeft, this.toScreenY(committed), columnWidth, this.toScreenHeight(committed));

            ctx.fillStyle = this.config.color.added;
            ctx.fillRect(firstColumnLeft, this.toScreenY(added) - this.toScreenHeight(committed), columnWidth, this.toScreenHeight(added));

            ctx.fillStyle = this.config.color.committedAndDone;
            ctx.fillRect(secondColumnLeft, this.toScreenY(committedAndDone), columnWidth, this.toScreenHeight(committedAndDone));

            ctx.fillStyle = this.config.color.addedAndDone;
            ctx.fillRect(secondColumnLeft, this.toScreenY(addedAndDone) - this.toScreenHeight(committedAndDone), columnWidth, this.toScreenHeight(addedAndDone));

            ctx.fillStyle = this.config.color.text;
            if (committed > 0) {
                ctx.fillText(`${committed}`, firstColumnLeft + (columnWidth - ctx.measureText(`${committed}`).width) / 2, this.toScreenY(committed) + this.toScreenHeight(committed) / 2 + 5)
            }
            if (added > 0) {
                ctx.fillText(`${added}`, firstColumnLeft + (columnWidth - ctx.measureText(`${added}`).width) / 2, this.toScreenY(added) - this.toScreenHeight(committed) + this.toScreenHeight(added) / 2 + 5)
            }
            if (committedAndDone > 0) {
                ctx.fillText(`${committedAndDone}`, secondColumnLeft + (columnWidth - ctx.measureText(`${committedAndDone}`).width) / 2, this.toScreenY(committedAndDone) + this.toScreenHeight(committedAndDone) / 2 + 5)
            }
            if (addedAndDone > 0) {
                ctx.fillText(`${addedAndDone}`, secondColumnLeft + (columnWidth - ctx.measureText(`${addedAndDone}`).width) / 2, this.toScreenY(addedAndDone) - this.toScreenHeight(committedAndDone) + this.toScreenHeight(addedAndDone) / 2 + 5)
            }
        });
        const averageVelocity = this.data.sprints
            .map(item => item.committedAndDone.length + item.addedAndDone.length)
            .reduce((accum, item) => accum + item, 0) / this.data.sprints.length;

        ctx.strokeStyle = this.config.color.averageVelocity;
        ctx.setLineDash([10, 10, 1, 10]);
        ctx.beginPath();
        ctx.moveTo(0, this.toScreenY(averageVelocity));
        ctx.lineTo(ctx.canvas.width, this.toScreenY(averageVelocity));
        ctx.stroke();
        ctx.closePath();
        ctx.setLineDash([]);
        ctx.fillStyle = this.config.color.text;
        ctx.fillText(`Average velocity: ${averageVelocity}`,
            ctx.canvas.width / 2 - ctx.measureText(`Average velocity: ${averageVelocity}`).width / 2,
            this.toScreenY(averageVelocity) - 5
        );

        this.element.querySelector('table').innerHTML = `<tbody>
            <tr>
                <th></th>
                ${this.data.sprints.map(sprint => `<th>${sprint.name}</th>`).join('')}
            </tr>
            <tr>
                <td>Committed (ratio)</td>
                ${this.data.sprints.map(sprint => `<td>${sprint.committed.length}
                    (${(100 * sprint.committed.length / (sprint.committed.length + sprint.added.length)).toFixed(0)} %)</td>`).join('')}
            </tr>
            <tr>
                <td>Not planned work (ratio)</td>
                ${this.data.sprints.map(sprint => `<td>${sprint.added.length}
                    (${(100 * sprint.added.length / (sprint.committed.length + sprint.added.length)).toFixed(0)} %)</td>`).join('')}
            </tr>
            <tr>
                <td>Committed and done (reliability)</td>
                ${this.data.sprints.map(sprint => `<td>${sprint.committedAndDone.length} 
                    (${(100 * sprint.committedAndDone.length / sprint.committed.length).toFixed(0)} %)</td>`).join('')}
            </tr>
            <tr>
                <td>Added and done</td>
                ${this.data.sprints.map(sprint => `<td>${sprint.addedAndDone.length}</td>`).join('')}
            </tr>
            <tr>
                <td>Velocity</td>
                ${this.data.sprints.map(sprint => `<td>${sprint.committedAndDone.length + sprint.addedAndDone.length}</td>`).join('')}
            </tr>
        </tbody>`
    }

    loadData(board, blocks) {
        this.data = {
            board,
            sprints: blocks.map(({sprint, data}) => {
                let rawData = Object.entries(data.changes)
                    .map(([timestamp, [entry]]) => {
                        return {...entry, timestamp: timestamp * 1}
                    })
                    .sort((a, b) => a.timestamp - b.timestamp);
                const {activatedTime, startTime} = data;
                let committed = rawData
                    .filter(item => item.added)
                    .filter(item => item.timestamp < activatedTime)
                    .map(item => item.key)
                    .reduce((accum, item) => {
                        if (!accum.includes(item)) {
                            accum.push(item);
                        }
                        return accum;
                    }, []);
                let added = rawData.filter(item => item.added)
                    .filter(item => item.timestamp > activatedTime)
                    .map(item => item.key)
                    .reduce((accum, item) => {
                        if (!accum.includes(item)) {
                            accum.push(item);
                        }
                        return accum;
                    }, []);
                return {
                    name: sprint.name,
                    data: rawData,
                    committed,
                    added,
                    startTime: activatedTime,
                    committedAndDone: rawData
                        .filter(item => item.column && item.column.notDone === false && item.timestamp > activatedTime)
                        .map(item => item.key)
                        .reduce((accum, item) => {
                            if (!accum.includes(item)) {
                                accum.push(item);
                            }
                            return accum;
                        }, [])
                        .filter(item => committed.includes(item)),
                    addedAndDone: rawData
                        .filter(item => item.column && item.column.notDone === false && item.timestamp > activatedTime)
                        .map(item => item.key)
                        .reduce((accum, item) => {
                            if (!accum.includes(item)) {
                                accum.push(item);
                            }
                            return accum;
                        }, [])
                        .filter(item => added.includes(item))

                }
            })
        }
    }

    /**
     * @param {CanvasRenderingContext2D} ctx
     */
    drawAxi(ctx) {
        ctx.beginPath();
        ctx.strokeStyle = this.config.color.axi;
        ctx.moveTo(0, 0);
        ctx.lineTo(0, ctx.canvas.height);
        ctx.lineTo(ctx.canvas.width, ctx.canvas.height);
        ctx.stroke();
        ctx.closePath();
    }

    toScreenHeight(units) {
        const screenDelta = this.canvas.height;
        const unitsDelta = Math.max(...this.data.sprints.map(item => item.committed.length + item.added.length));

        return units * screenDelta / unitsDelta;
    }

    toScreenY(units) {
        const screenDelta = this.canvas.height;
        const unitsDelta = Math.max(...this.data.sprints.map(item => item.committed.length + item.added.length));

        const screenY = units * screenDelta / unitsDelta;

        return this.canvas.height - screenY;
    }

    appendTo(parent) {
        parent.appendChild(this.element);
    }
}